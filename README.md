# 14 - Introdução ao Nodejs com Typescript

> **Objetivo**: Aprender a utilizar o NodeJS com TypeScript

## Por que usar TypeScript?

TypeScript cresceu muito nos últimos anos e cada vez mais empresas têm adotado ele como um padrão para uso com JavaScript em escala corporativa pelos benefícios que ele traz em termos de organização e padronização de código, além de segurança e qualidade.

Embora seja uma tecnologia polêmica, uma vez que tira parte da produtividade e liberdade fornecida pela tipagem dinâmica, é certo que para grandes projetos e/ou grandes times os seus benefícios no médio e longo prazo superam o drawback no curto prazo.

## Iniciar um projeto Nodejs Express com Typescript

### Criar um novo projeto

```bash	
npm init
```

### Instale o pacote typescript

O mecanismo Node.js executa Javascript e não Typescript. O pacote *Typescript node* permite que você transpile seus `.ts` arquivos para `.js` scripts. O Babel também pode ser usado para transpilar Typescript, porém, o padrão do mercado é usar o pacote oficial da Microsoft.

```bash
npm install typescript
```

Atualize o arquivo `package.json` adicionando `tsc` na tag de scripts para chamar funções de typescript a partir da linha de comando

```json
“scripts”: { 
  ..., 
  “tsc”: “tsc”,
}
```

Agora execute o comando abaixo:

```bash
npx tsc --init
```

Este comando inicializa o projeto typescript criando o tsconfig.json.

### Instalar o Express

```bash
npm install express @types/express
```

Por padrão, o Typescript não “conhece” tipos de classes Express. Existe um pacote npm específico para o Typescript reconhecer os tipos do Express.

### Criar arquivo Server.ts (server/server.ts)

```typescript	
import express = require('express');

// Cria uma nova instância de aplicativo express
const app: express.Application = express();

app.get('/', function (req, res) {
  res.send('Olá Mundo!');
});

app.listen(3000, function() {
  console.log('App está escutando na porta 3000!');
});
```

Compile o código acima executando o comando abaixo:

```bash
npm run tsc
```

Depois de executar o comando acima, um novo arquivo é criado na pasta do servidor chamada `server.js` (principalmente com o código *TS* é convertido em *JS*)

Execute o aplicativo:

```bash
node server/server.js
```

Verifique no navegador na URL: http://localhost:3000

## Interface e Classes

### Interfaces
Uma interface é, em essência, um tipo literal de objeto nomeada, ou seja, define a estrutura de um objeto.
Exemplo:

```ts
interface IPerson {
  name: string
  age: number | string
}
```

Com esta interface podemos falar que um objeto do tipo `IPerson` pode ser de duas maneiras:

```ts
const person: IPerson = {
  name: 'John',
  age: 20,
}

const person2: IPerson = {
  name: 'Joe',
  age: '21',
}
```

Podemos extender classes para reaproveitar campos já exitentes, exemplo:

```ts
interface IStudent extends IPerson {
  school: string
}

const student: IStudent = {
  name: 'John',
  age: 20,
  school: 'Fatec',
}
```

Podemos também definir itens não obrigatórios com `?`, exemplo:
```ts
interface IPerson {
  name: string
  age: number
  city?: string
}

const person: IPerson = {
  name: 'John',
  age: 20,
}

const person2: IPerson = {
  name: 'Joe',
  age: 21,
  city: 'São Paulo',
}
```

### Classes

Classes em JavaScript são introduzidas no ECMAScript 2015 e são simplificações da linguagem para as heranças baseadas nos protótipos. A sintaxe para classes não introduz um novo modelo de herança de orientação a objetos em JavaScript. Classes em JavaScript provêm uma maneira mais simples e clara de criar objetos e lidar com herança.

O sistema de classes no TypeScript usa um modelo de herança única que deve ser familiar a qualquer programador que tenha trabalhado com qualquer linguagem baseada em classes.

Exemplo:

```ts
class Person {
  public name: string
  public age: number

  constructor(name: string, age: number) {
    this.name = name
    this.age = age
  }

  public showMe() {
    console.log(`Hello, I am ${this.getName()}, and I am ${this.age} years old`)
  }

  private getName(): string {
    return this.name
  }
}

const person = new Person('John', 20)
person.showMe() // result => Hello, I am John and I am 20 years old
```

Classes em TypeScript pode também definir as propriedades como sendo `public`, `private`, `protected` e/ou `static`:
- **public:** Permite acessar a propriedade livremente.
- **private:** Quando uma propriedade é marcada como `private`, ela não pode ser acessada de fora da classe que o contém.
- **protected:** O modificador `protected` age como o `private`, com a exceção de que os membros declarados como `protected` também podem ser acessados ​​nas classes derivadas.  
- **static:** São visíveis na própria classe e não nas instâncias.

## Docs:
- [Classes JS ES6](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Classes)
- [Classes TypeScript](https://www.typescriptlang.org/docs/handbook/classes.html)
- [Interfaces TypeScript](https://www.typescriptlang.org/docs/handbook/interfaces.html)
- [Write Node server with ES6 Classes](https://dev.to/yuribenjamin/write-node-server-with-es6-classes-3b0j)
- [Getting Started Using TypeScript with Node.js and Express](https://medium.com/@pankaj.itdeveloper/getting-started-using-typescript-with-node-js-and-express-6aff573667d5)
- [Configurando Node.js com TypeScript, nodemon e Jest](https://danieldcs.com/configurando-node-js-com-typescript-nodemon-e-jest/)
- [TypeScript com NodeJS do jeito certo](https://youtu.be/aTf8QTjw4RE)

## Desafio

### Entrega:

Vocês deverão criar um endpoint que converte um JSON pra XML, com base no retorno da API **Via CEP**, utilizando a **Pesquisa de CEP**:

- `https://viacep.com.br/ws/{{estado}}/{{cidade}}/{{logradouro}}/json/`
- [Via CEP](https://viacep.com.br/)

Para isso, vocês criarão um middleware que faz o `GET` na API **Via CEP**, um middleware que converte para XML e a rota que retorna o XML.

A rota deve conter um dos seguintes caminhos: 
- `localhost:3000/{{estado}}/{{cidade}}/{{logradouro}}`
- `localhost:3000?estado={{estado}}&cidade={{cidade}}&logradouro={{logradouro}}`

O XML deverá ser um item baixável ao ser acessado pelo navegador, para isso, vocês faram o uso dos `headers`, e deverão usar no mínimo 2 dos headers apresentados em sala, para a aplicação funcionar corretamente.

**Obs.:** Para o desafio, desconsiderem os headers `Cookie` e `Set-Cookie`.

Os headers deverão ter os valores corretos, de acordo a resposta do endpoint.

Para converter para XML, vocês podem usar bibliotecas do npm.

É obrigatório a criação de uma pasta desafio contendo os scripts.

Com TypeScript:
- Tipar corretamente o retorno da API **Via CEP** com `interfaces`.
- Conter um método para buscar os ceps, um método com os middlewares e um método para iniciar o servidor.

Links úteis para TypeScript:
- [Getting Started Using TypeScript with Node.js and Express](https://medium.com/@pankaj.itdeveloper/getting-started-using-typescript-with-node-js-and-express-6aff573667d5)
- [Configurando Node.js com TypeScript, nodemon e Jest](https://danieldcs.com/configurando-node-js-com-typescript-nodemon-e-jest/)

Leiam este artigo para entender como passar informações de um middleware para outro:
- [Iniciando com middlewares no Express.js](https://blog.rocketseat.com.br/middlewares-no-express-js/)

### Para se destacar:

Conter uma classe com no mínimo um método `private`.

Criar rotas do tipo `get` com parâmetros que possam retornar os seguintes tipos, cada um com seus respectivos headers:

- um `xml` baixável
- um `csv` baixávels
- um `json`

Criar tratativas de erros e exceções.
